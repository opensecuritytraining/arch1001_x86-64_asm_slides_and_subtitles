1
00:00:00,240 --> 00:00:08,160
OST apresenta Brendan Fraser em

2
00:00:03,520 --> 00:00:08,160
jornada até o centro da memcpy

3
00:00:08,240 --> 00:00:11,519
portanto

4
00:00:10,320 --> 00:00:14,000
JornadaParaOCentroDeMemcpy.c

5
00:00:11,519 --> 00:00:14,960
vamos nos aprofundar em memcpy

6
00:00:14,000 --> 00:00:18,240
olhando a

7
00:00:14,960 --> 00:00:20,640
instrução elusiva rep movs

8
00:00:18,240 --> 00:00:21,920
portanto, temos uma estrutura aqui que

9
00:00:20,640 --> 00:00:24,800
pacote de pragma 1

10
00:00:21,920 --> 00:00:26,800
para esmagar tudo isso, temos 2

11
00:00:24,800 --> 00:00:30,400
instâncias da estrutura que nós

12
00:00:26,800 --> 00:00:32,320
definir uma única variável como 0xff e, em seguida

13
00:00:30,400 --> 00:00:34,000
memcpy de a para b para que possamos

14
00:00:32,320 --> 00:00:37,360
confirmar quando ele for realmente copiado

15
00:00:34,000 --> 00:00:40,719
é claro que retornamos 0xAce0fBa5e

16
00:00:37,360 --> 00:00:43,360
portanto, aqui está a assembly e eu quero que você

17
00:00:40,719 --> 00:00:45,200
passo para o desconhecido. memcpy é uma

18
00:00:43,360 --> 00:00:46,000
função de biblioteca esta não é nossa fonte

19
00:00:45,200 --> 00:00:47,840
de codificação

20
00:00:46,000 --> 00:00:49,039
mas quero que você entre no

21
00:00:47,840 --> 00:00:52,079
vista do assembly

22
00:00:49,039 --> 00:00:52,800
e cavar nele até encontrar o rep

23
00:00:52,079 --> 00:00:54,719
movs

24
00:00:52,800 --> 00:00:56,960
portanto, o rep movs é o assembly

25
00:00:54,719 --> 00:00:58,879
e as instruções que estamos buscando aqui

26
00:00:56,960 --> 00:01:00,719
acabamos de aprender sobre os rep stos e assim por diante

27
00:00:58,879 --> 00:01:01,440
isso estará de alguma forma relacionado a

28
00:01:00,719 --> 00:01:03,920
isso

29
00:01:01,440 --> 00:01:05,760
agora tradicionalmente na aula presencial

30
00:01:03,920 --> 00:01:08,080
Eu tinha apenas, você sabe, levado as pessoas

31
00:01:05,760 --> 00:01:09,200
através do código-fonte e explicou

32
00:01:08,080 --> 00:01:11,360
em slides

33
00:01:09,200 --> 00:01:12,960
isso é necessário porque, normalmente, esse

34
00:01:11,360 --> 00:01:14,479
exemplo vem no final da

35
00:01:12,960 --> 00:01:15,920
aula e preciso me certificar de que recebemos o 

36
00:01:14,479 --> 00:01:17,840
toda a aula foi concluída

37
00:01:15,920 --> 00:01:19,200
mas é muito melhor para o seu aprendizado

38
00:01:17,840 --> 00:01:21,520
experiência se

39
00:01:19,200 --> 00:01:23,439
aqueles de vocês que são ousados agora

40
00:01:21,520 --> 00:01:24,159
optar por fazer esse exercício

41
00:01:23,439 --> 00:01:27,200
você mesmo

42
00:01:24,159 --> 00:01:29,119
antes de lhe mostrar a solução, então o que eu

43
00:01:27,200 --> 00:01:31,759
preciso que você pare

44
00:01:29,119 --> 00:01:32,240
percorrer o assembly memcpy e

45
00:01:31,759 --> 00:01:35,280
encontrar

46
00:01:32,240 --> 00:01:37,360
os rep movs agora, em particular, você está

47
00:01:35,280 --> 00:01:39,040
provavelmente vai precisar, mas não devo

48
00:01:37,360 --> 00:01:39,520
dizer que provavelmente você vai

49
00:01:39,040 --> 00:01:42,399
precisar

50
00:01:39,520 --> 00:01:44,240
para modificar a estrutura a fim de forçar o

51
00:01:42,399 --> 00:01:46,159
o assembly memcpy

52
00:01:44,240 --> 00:01:48,399
no caminho certo que, em última análise

53
00:01:46,159 --> 00:01:51,040
leva você ao rep movs

54
00:01:48,399 --> 00:01:53,040
e para torná-lo ainda mais desafiador

55
00:01:51,040 --> 00:01:54,159
Quando você finalmente encontrar os rep movs, eu

56
00:01:53,040 --> 00:01:57,439
quero que você escreva

57
00:01:54,159 --> 00:02:00,640
pseudocódigo do caminho que leva a ele

58
00:01:57,439 --> 00:02:09,840
muito bem, agora aventureiros ousados

59
00:02:00,640 --> 00:02:09,840
mais uma vez no abismo

60
00:02:10,160 --> 00:02:12,239
você

