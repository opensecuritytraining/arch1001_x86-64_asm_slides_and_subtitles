Slides and subtitle files for OpenSecurityTraining ["Architecture 1001: x86-64 Assembly"](https://ost2.fyi/Arch1001) class originally created by Xeno Kovah

Corrections to the subtitles are always welcome via merge requests
